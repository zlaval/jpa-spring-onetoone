package com.zlrx.demo.controller

import com.zlrx.demo.domain.Account
import com.zlrx.demo.domain.GmLevel
import com.zlrx.demo.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class DemoController {

    @Autowired
    lateinit var repository: AccountRepository

    @GetMapping("/add")
    fun addAccount(): Account {
        var gmLevel = GmLevel()
        gmLevel.gmLevel = 7
        var account = Account()
        account.gmLevel = gmLevel
        return repository.save(account)
    }

    @GetMapping("/get")
    fun getAccounts(): List<Account> {
        return repository.findAll()
    }


}