package com.zlrx.demo.domain

import javax.persistence.*

@Table
@Entity
class GmLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    @Basic
    var gmLevel: Int = 0

}