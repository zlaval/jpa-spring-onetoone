package com.zlrx.demo.domain

import javax.persistence.*

@Table
@Entity
class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    @OneToOne(cascade = [(CascadeType.ALL)])
    @JoinColumn(name = "gm_level_id", referencedColumnName = "id")
    var gmLevel: GmLevel? = null

}

