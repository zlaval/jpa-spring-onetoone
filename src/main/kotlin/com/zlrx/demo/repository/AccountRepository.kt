package com.zlrx.demo.repository

import com.zlrx.demo.domain.Account
import org.springframework.data.jpa.repository.JpaRepository

interface AccountRepository : JpaRepository<Account, Long>